from django.db import models


# Create your models here.
class Scrappy(models.Model):
    title = models.CharField(max_length=500)
    news = models.TextField(blank=True)
    image=models.CharField(max_length=200)
    date=models.DateField(auto_now=True)
    link=models.CharField(max_length=200)
