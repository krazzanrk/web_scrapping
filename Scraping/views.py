from django.shortcuts import render
from bs4 import BeautifulSoup
import requests as req
from datetime import date

# Create your views here.
from django.views import View

from Scraping.models import Scrappy


class ScrappyView(View):

    def get(self, request):

        page = req.get('http://www.nepalihealth.com/')
        page_parsed = BeautifulSoup(page.content, 'html.parser')

        a_tags = page_parsed.find_all('a')
        links = set()

        today = str(date.today())
        print(today)
        year = today[0:4]
        month = today[5:7]
        day = today[8:10]


        for a_tag in a_tags:

            if '/' + year + '/' + month + '/' + day + '/' in a_tag.get('href', ''):
                links.add(a_tag.get('href'))
                # up to now adding link from a_tag to links

        for link in links:
            if not Scrappy.objects.filter(link=link).exists():

                steal = Scrappy()
                single_news = req.get(link)
                single_news_parsed = BeautifulSoup(single_news.content, 'html.parser')

                topic = single_news_parsed.find('h1').text
                print(topic)
                paragraph = single_news_parsed.find(class_='entry-content')
                detailnews = paragraph.find_all('p')
                img = paragraph.find_all('img')
                p_list = []
                for ii in img:
                    single_photo = ii
                    p_list.append(single_photo['src'])
                q_list = p_list[1:2]
                photo = '\n'.join(q_list)
                print(detailnews)
                clean_news = []
                for i in detailnews:
                    detailnews = i.text
                    clean_news.append(detailnews)
                list = '\n'.join(clean_news)

                steal.title = topic
                steal.news = list
                steal.image = photo
                steal.link = link
                steal.save()

        template_context = {
            'scraps': Scrappy.objects.all()
        }

        return render(request, 'index.html', template_context)
